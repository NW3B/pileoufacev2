<?php

namespace App\Controller;

use App\Form\QuestionType;
use App\Service\OpenAIService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(Request $request, OpenAIService $openAI): Response
    {
        $form = $this->createForm(QuestionType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $choix1 = $data['choix1'];
            $choix2 = $data['choix2'];

            $rand = rand(0, 1);
            
            if ($rand == 0) {
                $choixFinal = $choix1;
            }

            if ($rand == 1) {
                $choixFinal = $choix2;
            }

            //var_dump(rand(0,1));
            $json = $openAI->getHistory($choixFinal);

            return $this->render('home/histoire.html.twig', [
                'json' => $json,
            ]);
        }

        return $this->render('home/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
